# Markdown Parser

## Acerca del proyecto
Este proyecto tiene el propósito de proporcionar una manera de publicar en internet, contenido textual de una manera simple, rápida y segura. Este proyecto consta de una aplicación web generada con la librería React, y un servidor configurado desde la web de gitlab.com. 

El proyecto pretende poner al alcance de su usuario herramientas y métodos para publicar contenido sin necesidad de realizar demasiadas tareas de instalación, configuración o administración que se llevan a cabo en los proyectos de desarrollo web más comunes. 

El proyecto también pretende ser lo suficientemente flexible para ofrecer la posibilidad de personalizar el tema de la interfaz web de la manera que más interese al usuario. Sabiendo nociones básicas de HTML/CSS, el usuario puede editar el diseño de la web sin demasiadas complicaciones, ya que el proyecto no depende de librerías con diseños estáticos de componentes.

Se presentará también una manera de publicar contenido web sin necesidad de realizar configuraciones adicionales que comúnmente se realizan en algunos otros servicios web.


## Componentes principales

El proyecto consta de tres elementos fundamentales para su funcionamiento:
- Código escrito en la librería React que genera la interfaz web para mostrar el contenido
- Repositorio Gitlab que aloja el código del proyecto
- Servidor que aloja el contenido en la web, configurado desde Gitlab Pages.

La documentación de cada uno se mostrará en secciones posteriores.

## Ejemplo de uso

Para demostrar un ejemplo de uso de este proyecto, se presenta el siguiente enlace: [https://atreston.gitlab.io/ialabmd/](https://atreston.gitlab.io/ialabmd/)

La web mostrará una página que contiene una lista de publicaciones y eventos que se alojan en forma de notas markdown dentro del repositorio del proyecto. La web se conforma de dos secciones principales: Publicaciones y Eventos.

La sección de Publicaciones es la página principal de la web, muestra una lista de los archivos que se alojan en un directorio del repositorio del proyecto. Esta lista se presenta en forma de tarjetas que contienen el título de la publicación, el autor y la fecha en la que fueron guardados en el repositorio del proyecto. Estas tarjetas a su ves son enlaces que al darles click, se muestra el contenido de dicho archivo markdown alojado en el repositorio. También se muestra una barra lateral que muestra eventos archivados en el repositorio, además de un recuadro de búsqueda de publicaciones.

La sección de Eventos muestra una lista de Eventos, archivados en forma de textos markdown, alojados dentro del repositorio del proyecto. Estos eventos se presentan en modo de tarjeta que muestra los detalles de dicho evento, especificados dentro del mismo archivo markdown. Cada tarjeta se muestra como un enlace que al ser presionado con un click, se muestran los contenidos del archivo markdown de dicha tarjeta.

## Configuración del repositorio del proyecto

Antes de arrancar el proyecto, será necesario realizar algunas configuraciones iniciales. En general, se requiere realizar las siguientes acciones:

- Copiar el proyecto en un repositorio propio
- Proteger el repositorio propio
- Realizar configuraciones iniciales en el repositorio.

### Copiar el proyecto

El proyecto se encuentra alojado en un repositorio de GitLab. Para poder activar GitLab Pages en un repositorio propio, será necesario crear un fork en el repositorio propio y después, remover el enlace al proyecto original.

Para crear el fork, primero nos dirigimos al repositorio original, localizamos y hacemos click en el botón `Forks`:

![image info](doc_img/scr_401.png)

Llenamos los campos conforme a las necesidades de nuestro proyecto, colocamos un nombre único para el proyecto (requerido para poder tener un dominio web personalizado) y elegimos un namespace y un identificador único para el proyecto (slug), éste último parámetro se utilizará para darle un nombre de dominio web más personalizado al proyecto:

![image info](doc_img/scr_402.png)

Configuramos la visibilidad de nuestro proyecto y al finalizar, hacemos click en `Fork project` :

![image info](doc_img/scr_403.png)

Con esto hemos copiado el proyecto a nuestra cuenta GitLab. Ahora será necesario eliminar los enlaces al proyecto original para que en un futuro, los cambios que hagamos al proyecto no se mezclen con el proyecto original (o viceversa).

### Proteger el repositorio

También es necesario proteger la rama del repositorio desde donde GitLab Pages tomará los recursos para configurar y desplegar el servicio web. 

Gitlab, por defecto, restringe a los usuarios de realizar `merge` o `push` a la rama `master`. Verificaremos si la rama `master` del repositorio esta con la debida configuración ingresando al menú `Settings > Repository`:

![image info](doc_img/scr_403a.png)

Después, expandimos la sección `Branch rules` y verificamos que la etiqueta `protected` esté presente:

![image info](doc_img/scr_403b.png)

En caso de no estarlo, cambiaremos la configuración desde la sección `Protected branches`. Daremos click en el botón `Add protected branch`:

![image info](doc_img/scr_403c.png)

En la opción `Branch`, seleccionamos `master` y en las opciones `Allowed to merge:` y `Allowed to push and merge:` seleccionamos `Maintainers`. Confirmamos los cambios haciendo click en `Protect`:

![image info](doc_img/scr_403d.png)

Para evitar que los cambios que hagamos al proyecto no afecten al proyecto original (y viceversa), removeremos la relación de nuestro proyecto con el fork. Nos dirigimos al menù `Settings > General`:

![image info](doc_img/scr_404.png)

Expandimos la sección `Advcanced`:

![image info](doc_img/scr_405.png)

Buscamos la sección `Remove fork relationship`, hacemos click en el botón y confirmamos los cambios:

![image info](doc_img/scr_406.png)

![image info](doc_img/scr_407.png)

Para más información sobre estos cambios, consúltese los siguientes enlaces:

* [Project forking workflow | GitLab](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
* [Unlink a fork | GitLab](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#unlink-a-fork)

### Configuraciones iniciales

Ahora es **importante** que realicemos los siguientes cambios para que nuestro proyecto pueda ser ejecutado por GitLab Pages. Debemos cambiar algunos parámetros para que el proyecto pueda leer los archivos en nuestro repositorio. Primero, localizamos el archivo `package.json`  ubicado en la raíz de nuestro repositorio y cambiaremos las siguientes líneas:

`package.json`
```json
{
	"homepage": "ialabmd", // cambiar nombre
	"name": "ialabmd", // cambiar nombre
	"version": "0.1.0",
	"private": true,
	"dependencies": {
	...
```

![image info](doc_img/scr_408.png)

Estas líneas deben coincidir con el parámetro `Project slug` que configuramos pasos atrás:

![image info](doc_img/scr_402.png)

Después, localizamos el archivo `hooks.jsx` ubicado en `src\hooks\hooks.jsx` y cambiamos el valor de la constante `REPO_ID` por el valor del ID de nuestro proyecto:

`src\hooks\hooks.jsx`
```jsx
...
const BASE_URL = 'https://gitlab.com/api/v4/projects'
const REPO_ID = '40069687' // cambiar ID
const REF = 'pages'
const FETCH_TREE = 'repository/tree'
const FETCH_FILES = 'repository/files'
const PATH_POSTS = 'publicaciones'
...
```

![image info](doc_img/scr_409.png)

![image info](doc_img/scr_410.png)

Con esto, terminaremos de configurar nuestro repositorio de proyecto. Ahora procederemos a configurar GitLab Pages.

## Configuración del servidor del proyecto

Para configurar GitLab Pages, primero tendremos que dirigirnos al menú `Deploy > Pages`:

![image info](doc_img/scr_501.png)

Al solicitar el nombre de la imagen para montar en servidor, colocaremos `node`, tildamos en donde dice *The app's built output files are in a folder named "public"* y después, copiamos dentro del recuadro rotulado como *Draft: .gitlab-ci.yml* el texto que está dentro del archivo `.gitlab.yml` que a su ves se aloja en la raíz de nuestro repositorio.

![image info](doc_img/scr_504.png)

![image info](doc_img/scr_505.png)

Una ves que se copió el código, hacemos click en siguiente:

![image info](doc_img/scr_506.png)

En los siguientes pasos no es necesario configurar nada. Hacemos click en siguiente hasta terminar:

![image info](doc_img/scr_507.png)

![image info](doc_img/scr_508.png)

Aquí simplemente le damos en `Commit`

![image info](doc_img/scr_509.png)

Al hacer click en `Commit`, GitLab comenzará a configurar y servir la página web del proyecto. Damos click en `Check the Pipeline Status` y veremos el estatus de la configuración:

![image info](doc_img/scr_510.png)

![image info](doc_img/scr_510a.png)

Mientras el proyecto se configura, podemos configurar el nombre de dominio de la web del proyecto. Nos dirigimos al menu `Deploy > Pages`:

![image info](doc_img/scr_512.png)

En este menú, tildamos la opción `Use unique domain` y después en `Save changes`:

![image info](doc_img/scr_512a.png)

Ahora, la web del dominio quedará con un nombre más personalizado:

![image info](doc_img/scr_514.png)

Con esto, el proyecto ya estará accesible desde internet.

## Como empezar a publicar

Para comenzar a publicar contenido, primero clonamos el repositorio propio en un directorio local:

```shell
git clone https://gitlab.com/User/repo_name.git
```

```shell
git clone git@gitlab.com:User/repo_name.git
```

Después, cambiaremos a la rama `pages`:

```shell
git checkout pages
```

El repositorio contendrá solo dos directorios:

- `publicaciones` es el directorio que contiene los archivos markdown que se cargan en la sección `Publicaciones` de la web.
- `eventos` es el directorio que contiene los archivos markdown para la sección de eventos.

Para realizar una publicación, basta con crear un nuevo archivo con extensión `md` en su respectivo directorio y realizar un commit y un push a los cambios.

```shell
git touch ./publicaciones/nueva_publicacion.md
```

`repo_name\publicaciones`
```
# Nueva publicación

```

```shell
git add *
git commit -m 'Nueva publicacion creada'
git push
```

Una vez que los cambios sean registrados en el repositorio propio en GitLab, la aplicación mostrara la nueva publicación al refrescar la página.

![image info](doc_img/scr_600.png)

## Código del proyecto

El proyecto está pensado para ejecutarse con una de las librerías más utilizadas (React) y usando el menor número de dependencias. El archivo `package.json` muestra las dependencias requeridas por el proyecto:

```js
"dependencies": {
	"@testing-library/jest-dom": "^5.16.5",
	"@testing-library/react": "^13.4.0",
	"@testing-library/user-event": "^13.5.0",
	"@uiw/react-markdown-preview": "^4.1.8",
	"dayjs": "^1.11.9",
	"react": "^18.2.0",
	"react-dom": "^18.2.0",
	"react-infinite-scroll-component": "^6.1.0",
	"react-router-dom": "^6.8.1",
	"react-scripts": "5.0.1",
	"web-vitals": "^2.1.4"
},
```

El módulo `@uiw/react-markdown-preview` es el encargado de renderizar el contenido de los archivos markdown en un formato mas legible. 

El módulo `dayjs` se encarga de transformar los formatos de hora y fecha de algunos archivos. 

El módulo `react-infinite-scroll-component` se encarga de presentar el conjunto de archivos cargados desde la api de GitLab en un formato más simple, en forma de scroll infinito.

El proyecto usa los módulos que se instalan por defecto junto con el comando `create-react-app`. A pesar de que es un sistema desactualizado, se ajusta bien a los requerimientos de GitLab Pages. Cualquier otro sistema puede requerir una configuración de complejidad mayor.

El proyecto usa la API de GitLab para cargar y mostrar los archivos mostrados de manera remota en el repositorio. Para más información sobre la API de GitLab, consulte el enlace siguiente:

* [REST API | GitLab](https://docs.gitlab.com/ee/api/rest/)

`useListPublications` es un custom hook de React que se encarga de extraer los datos necesarios de los archivos almacenados en el repositorio:

```jsx
useListPublications({ setFileList: setPublications })
useListPublications({ setFileList: setEvents, path: 'eventos' })
```

El proyecto usa `react-router-dom` para simular la estructura de enrutamiento de URLs de una página común. GitLab Pages requiere que la base de la URL sea el nombre del repositorio configurado en el campo Slug de la configuración del nombre del repositorio. El proyecto puede extraer este parámetro (que el usuario tendrá que configurar) desde el archivo `package.json` :

```json
{
	"homepage": "ialabmd", // nombre base de la url
	"name": "ialabmd",
	"version": "0.1.0",
	"private": true,
	"dependencies": {
	...
```

Este parámetro es leído por la siguiente línea:

```jsx
<BrowserRouter basename={process.env.PUBLIC_URL}>
```

La aplicación web tiene dos rutas principales: `/publicaciones` y `/eventos`:

`src\App.jsx`
```jsx
<Route path='/' ... >
	<Route index element={...} />
	
	<Route path='/publicaciones'>
		<Route path=':fileName' element={...} />
	</Route>
	
	<Route path='/eventos' element={...}/>
	
	<Route
		path='/eventos/:fileName' element={...} />
	<Route path='/acerca' element=''/>
</Route>
```

La interfaz web muestra en la barra lateral, los eventos almacenados en los archivos del repositorio (componente `<Events />`) y la lista de publicaciones (componente `<Publications />`):

`src\components\home\Home.jsx`
```jsx
<div ... >
	<Events ... />
	<main ... >
		<Publications ... />
		...
	</main>
	<div ... ></div>
</div>
```

La lista de publicaciones se muestra en forma de una tarjeta que muestra los detalles de la publicación, como el nombre del autor (quién realizó el commit del archivo al repositorio), la fecha de creación y el nombre de la publicación:

`src\components\posts\Publications.jsx`
```jsx
...
<PostList pageList={pageList} hasMore={hasMore} nextPage={nextPage}>
	<PostCard />
</PostList>
```

La lista de eventos se muestra en forma de breve sumario con fecha, nombre y titulo:

`src\components\posts\Events.jsx`
```jsx
{events.length > 0?
	events.map( (item, i) => (
		<Link className='flex w-full md:w-5/6' key={i} to={`${item.path.slice(8)}`}>
			<EventCard key={i} item={item} />
		</Link>
	))
	:
	<div>No hay eventos</div>
}
```

La aplicación extrae la lista de archivos markdown de la api y las distribuye en los diferentes módulos usando React `props` . Se eligió esta aproximación en lugar de `useContext` debido a que la aplicación no es demasiado compleja y la codificación de React `props` no es muy complicada:

`src\App.jsx`
```jsx
function App() {
	const [eventList, setEvents] = useState([])
	const [publications, setPublications] = useState([])
	
	useListPublications({ setFileList: setPublications })
	useListPublications({ setFileList: setEvents, path: 'eventos' })
	...
```
```jsx
	<Home events={eventList} publications={publications} />
	<MdPost currentDir={'publicaciones'} events={eventList} />
	<Events events={eventList} />
	<MdPost currentDir={'eventos'} events={eventList} />
```

El componente de scroll infinito requiere de una lista que guardará y mostrara un listado pequeño de publicaciónes. El

`src\components\posts\PostList.jsx`
```jsx
<div ... >
	{!pageList
		? <LoadingSpinner ... />
		: Array.isArray(pageList)
			? <InfiniteScroll
				dataLength={...}
				next={() => nextPage()}
				hasMore={hasMore()}
				loader={...}
				scrollableTarget={overflowRef}
				endMessage={<p ...>No hay más publicaciones</p>}
			  >
				{pageList.map(
					(item, i) => (
						<Link ... to={`${item.path}`} >
							{ cloneElement(children, {key:i, markdown:item}) }
						</Link>
					)
				)}
			</InfiniteScroll>
			: <div ... >No hay publicaciones</div>
	}
</div >
```

`src\components\mdPost\MdPost.jsx`
```jsx
<div ... >
	<div .... >
		<Events events={events} />
	</div>
	
	<main ... >
		{loading ? 
			<div ... >
				<LoadingSpinner .../>
			</div>
			:
			<RenderMd
				source={markdown}
				title={fileName}
				currentDir={currentDir}
				setHeadings={setHeadings}
			/>
		}
	</main>
	
	<RightSidebar headings={headings}/>
</div>
```

`src\components\mdPost\react-markdown-preview\RenderMd.jsx`
```jsx
<div>
	<h1 ... >{trimFileExtension(title)}</h1>
	
	<MarkdownPreview
		className='markdownView'
		source={source}
		transformImageUri={(src) => {
			if (isURL(src)) return src
			else return `${BASE_URL}/${BRANCH}/${currentDir}/${src}`
		}}
	/>
</div>
```

