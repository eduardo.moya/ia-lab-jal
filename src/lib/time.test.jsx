import { fechaEN } from './time'

describe('Testing dates in ES', () => {
	test('ES to EN dates 01', () => {
		const date = new Date(fechaEN('1 Enero 2021'))
		expect(date).toBeInstanceOf(Date)
	})
	test('ES to EN dates 02', () => {
		const date = new Date(fechaEN('1 enero 2021'))
		expect(date).toBeInstanceOf(Date)
	})
	test('ES to EN dates 03', () => {
		const date = new Date(fechaEN('1-Febrero-2021'))
		expect(date).toBeInstanceOf(Date)
	})
	test('ES to EN dates 04', () => {
		const date = new Date(fechaEN('1/Septiembre/2021'))
		expect(date).toBeInstanceOf(Date)
	})
	test('ES to EN dates 05', () => {
		const date = new Date(fechaEN('1 octubre 2021'))
		expect(date).toBeInstanceOf(Date)
	})
	test('ES to EN dates 06', () => {
		const date = new Date(fechaEN('1 nov 21'))
		expect(date).toBeInstanceOf(Date)
	})
	test('ES to EN dates 07', () => {
		const date = new Date(fechaEN('1-dic-21'))
		expect(date).toBeInstanceOf(Date)
	})
})