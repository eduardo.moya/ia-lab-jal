import { parseEvent, trimFileExtension } from "./text"

test('trimFileExtension with empty', () => {
	expect(trimFileExtension('')).toBeUndefined()
})
test('trimFileExtension with no spaces', () => {
	expect(trimFileExtension('markdown.md')).toBeDefined()
})
test('trimFileExtension with a space', () => {
	expect(trimFileExtension('About markdown.md')).toBeDefined()
})
test('trimFileExtension with dashes', () => {
	expect(trimFileExtension('with-strange-characters.md')).toBeDefined()
})
test('trimFileExtension with non alphanumeric', () => {
	expect(trimFileExtension('with-strange-characters?.md')).toBeDefined()
})
test('trimFileExtension with double dot', () => {
	expect(trimFileExtension('test@test.com.md')).toBeDefined()
})
test('trimFileExtension with lower dashes', () => {
	expect(trimFileExtension('with_lower_dash.md')).toBeDefined()
})
test('parsing markdown: Fecha', () => {
	expect(parseEvent(markdown())).toHaveProperty('Lugar')
})
test('parsing markdown: Lugar', () => {
	expect(parseEvent(markdown())).toHaveProperty('Fecha')
})
test('parsing markdown: Participantes', () => {
	expect(parseEvent(markdown())).toHaveProperty('Participantes')
})
test('parsing markdown: Descripción', () => {
	expect(parseEvent(markdown())).toHaveProperty('Descripción')
})

function markdown() {
	return `# Cosechar Lechugas
## Fecha
21 jul 23
## Lugar
CCD
## Participantes
Marcos Martínez
## Descripción
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ac turpis egestas sed tempus urna et pharetra pharetra. Ac feugiat sed lectus vestibulum. Aliquam nulla facilisi cras fermentum odio eu. Consequat id porta nibh venenatis cras sed felis eget. Viverra aliquet eget sit amet tellus cras adipiscing. Commodo elit at imperdiet dui accumsan sit amet nulla facilisi. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Dui id ornare arcu odio ut sem nulla pharetra. Blandit turpis cursus in hac habitasse platea dictumst quisque. Facilisis gravida neque convallis a cras semper auctor neque. Fringilla urna porttitor rhoncus dolor purus non. Orci ac auctor augue mauris. Duis ut diam quam nulla porttitor. In cursus turpis massa tincidunt dui. Turpis in eu mi bibendum neque egestas. Non tellus orci ac auctor augue mauris augue. Ante metus dictum at tempor commodo ullamcorper a lacus vestibulum.
`
}

