
export const regexMarkdownFileName = /([_a-zA-Z0-9\s-]+[.,-_]*[_a-zA-Z0-9\s-]*)\.md/

export const regexEventMd = /^# (?<titulo>[\wÀ-ÿ\u00f1\u00d1\u2010-\u2015\u2018-\u201f\u2100-\u214f\u2200-\u22FF“”"$%^*()=\s]+\n)|## ([\wÀ-ÿÀ-ÿ\u00f1\u00d1]*)\n+([\w\sÀ-ÿÀ-ÿ\u00f1\u00d1<>\u00ab\u00bb,'().:;]*)/ig

export const regexDate = /(ene|abr|ago|dic|enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre)/gi

export function isNonEmptyString(string) {
	// from Clean Code in JavaScript - Padolsey
	return typeof string === 'string' && string.trim().length > 0;
}

export const trimFileExtension = input => {
	try {
		if(!isNonEmptyString(input)) throw Error('Empty file name')
		return input.substring(0, input.lastIndexOf('.'))
	} catch (error) {
		console.error(error)
	}
}

export const trimFileName = input => {
	try {
		if(!isNonEmptyString(input)) throw Error('Empty file name')
		return input.length > 23 ? `${input.substring(0, 20)}...` : trimFileExtension(input)
	} catch (error) {
		console.error(error)
	}
}

export const trimFileNameN = (input='',length='') => input.length > length ? `${input.substring(0, length)}...` : trimFileExtension(input)

export const parseEvent = markdown => {
	try {
		if(!isNonEmptyString(markdown)) throw Error('No markdown text to parse')
		const matches = markdown.match(regexEventMd)
		const result = Object.fromEntries(
			matches.map( item => item.split('\n') )
				.map( arr => arr.filter(item => item) )
				.map( arr => {
					if(arr.length === 1) arr.unshift('título')
					return arr.map(item => item.replace('# ', ''))
						.map(item=>item.replace('#',''))
				})
				.map(arr => arr.length > 2 ? [arr[0], arr.slice(1)] : arr )
		)
		return result
	} catch (error) {
		console.warn(error)
	}
}
