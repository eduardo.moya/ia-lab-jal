import { Link } from 'react-router-dom'
import EventCard from '../postcard/EventCard'

export default function Events({ events }) {

	return (
		events === null || events === undefined || events.length < 1 ?
			<p>No hay eventos</p>
			:
			<div className='flex flex-col gap-4 px-3 my-3 items-center self-center md:w-5/6'>
				{events.length > 0?
					events.map( (item, i) => (
						<Link className='flex w-full md:w-5/6' key={i} to={`${item.path.slice(8)}`}>
							<EventCard key={i} item={item} />
						</Link>
					))
					:
					<div>No hay eventos</div>
				}
			</div>
		
	)
}
