import EventCard from './EventCard'

export default function Events({ events, showSidebar, setShowsidebar }) {

	return (
		<>
			<div className={`fixed xl:static ${showSidebar? 'flex bg-slate-100 dark:bg-[#0d1117]' : 'hidden'} xl:flex top-0 xl:top-auto z-30 w-full xl:w-72 h-screen xl:h-auto justify-center overflow-auto`}>
				{events === null || events === undefined || events.length < 1 ? 
					<p>No hay eventos</p>
					:
					<nav className='min-w-[250px] max-w-[800px] px-2'>
						<h3 className='w-full px-2 py-1 border-b-2 border-slate-500'>
							Próximos Eventos
						</h3>
						
						<div id='eventos_proximos' className='divide-y divide-dashed'>
							{events.length > 0 ?
								events.map( (item, i) => (<EventCard key={i} item={item} />))
								:
								<p>No hay eventos</p>
							}
						</div>
					</nav>
				}
			</div>
			
			{ showSidebar !== undefined &&
				<button className='fixed flex z-40 xl:hidden bottom-3 left-3 lg:bottom-6 lg:left-6 p-2 bg-gray-600'
					onClick={() => setShowsidebar( current => !current)}
				>
					Events
				</button>
			}
		</>
	)
}
