import { useRef, useState } from "react"
import { useFiles } from "../../context/context"
import { MagnifyingGlass } from "../../lib/svg"
import { trimFileNameN } from "../../lib/text"
import { Link } from "react-router-dom"

export function SearchBox() {
	const { fileList } = useFiles()
	const inputRef = useRef()
	const [filteredItems, setFilteredItems] = useState([])

	function handleInput(e) {
		e.preventDefault()
		const input = inputRef.current.value
		if (input.length < 3) return setFilteredItems([])
		const results = fileList.filter(item => item.name?.toLowerCase().includes(input.toLowerCase()))
		setFilteredItems(results)
	}

	function handleLink() {
		setFilteredItems([])
		inputRef.current.value = ''
	}

	return (
		<div className="flex relative">
			<div className="relative w-full">
				<div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
					<MagnifyingGlass className={'w-5 h-5 fill-slate-500'} viewBox={'0 0 20 20'} />
				</div>

				<input
					className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2"
					id="search"
					ref={inputRef}
					type="search"
					placeholder="Buscar..."
					onChange={handleInput}
				/>
			</div>
			{filteredItems.length !== 0 && (
				<div className={`${filteredItems.length ? '' : 'hidden'} dataResult absolute top-10 rounded-lg mt-1 w-full h-auto bg-gray-700 shadow-md overflow-hidden overflow-y-auto z-50`}
				>
					{filteredItems.map((item, key) => (
						<Link key={key} to={`${item.path}`}>
							<div
								key={key}
								className='dataItem flex items-center w-full h-10 pl-4 hover:bg-gray-500 hover:cursor-pointer'
								onClick={handleLink}
							>
								{trimFileNameN(item.name, 33)}
							</div>
						</Link>
					))}
				</div>
			)}
		</div>
	)
}
