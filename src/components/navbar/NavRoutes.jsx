import { Link } from 'react-router-dom'

export function NavRoutes({ routes = [] }) {
	return (
		<div className="hidden md:flex justify-between items-center w-full md:w-auto md:order-1">
			<ul className="flex-col md:flex-row flex md:space-x-8 mt-4 mr-7 md:mt-0 md:text-sm md:font-medium">
				{routes.map((route, index) => (
					<li key={index}>
						<Link to={route.link}>
							<p className="text-lg text-white hover:bg-gray-50 border-b border-gray-100 md:hover:bg-transparent md:border-0 block mx-3 pl-3 pr-4 py-2 md:hover:text-blue-700 md:p-0">
								{route.name}
							</p>
						</Link>
					</li>
				))}
			</ul>
		</div>
	)
}
