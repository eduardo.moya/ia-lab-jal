// import { RenderMd } from './mardown-to-jsx/RenderMd'
import { RenderMd } from './react-markdown-preview/RenderMd'
import { useParams } from 'react-router-dom'
import RightSidebar from '../sidebar/RightSidebar'
import { LoadingSpinner } from '../../lib/svg'
import { useGetMarkdown } from '../../hooks/hooks'
import { useState } from 'react'
import Events from "../home/Events"

export default function MdPost({ currentDir, events }) {
	const { fileName } = useParams()
	const [markdown, setMarkdown] = useState('')
	const [loading, setLoading] = useState(true)
	const [headings, setHeadings] = useState([])
	useGetMarkdown({ fileName, setMarkdown, setLoading, path: currentDir })

	return (
		<div className='flex flex-wrap flex-1 w-full'>
			<div className='hidden lg:block md:w-1/5 bg-white dark:bg-[#0d1117]'>
				<Events events={events} />
			</div>

			<main className='w-full md:w-4/5 lg:w-3/5 bg-white dark:bg-transparent'>
				{loading ? 
					<div className='flex w-full h-full m-auto'>
						<LoadingSpinner 
							className='flex-1 m-auto w-10 h-10 animate-spin fill-blue-400 dark:fill-slate-100' 
							viewBox='0 0 20 20' 
						/>
					</div>
					:
					<RenderMd
						source={markdown}
						title={fileName}
						currentDir={currentDir}
						setHeadings={setHeadings}
					/>
				}
			</main>

			<RightSidebar headings={headings}/>
		</div>
	)
}
