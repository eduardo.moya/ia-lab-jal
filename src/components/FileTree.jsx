import { useState } from "react"

export const FileTree = () => {
	const [fileTree] = useState(null)

	return (
		<div>
			<ul>
				{fileTree?
					fileTree?.map((item, i) => {
						return <li key={i}>{item.path}</li>
					})
					:
					<p></p>
				}
			</ul>
		</div>
	)
}
